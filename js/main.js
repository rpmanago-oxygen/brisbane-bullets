$(document).ready(function(){	
	// registration form

	$('.selectpicker').selectpicker({
      	
	});

	$('.num-only').keypress(function(e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	$('#form-registration').validate({
        rules: {
        	firstname: {
        		required: true
        	},
        	surname: {
        		required: true
        	},
        	email: {
        		email: true,
        		required: true
        	},
        	mobile: {
        		number: true
        	},
        	postcode: {
        		number: true
        	}
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler: function (form) {
        	var $btn = $('#btn-registration-submit').button('loading');

        	var json = $('#form-registration').serializeArray();
        	console.log(json);

        	$('#form-registration')[0].reset();
        	$('.selectpicker').selectpicker('render');
        	$btn.button('reset');
        	
        	/*
            var $btn = $('#btn-settings-submit').button('loading');
            var json = $('#form-settings').serializeArray();

            $(json).each(function(i) {                
                if(json[i].name == "ticketlink"){
                    var ticketlink = json[i].value;
                    if(ticketlink.indexOf('http') == -1 && ticketlink.indexOf('https') == -1){
                        json[i].value = 'http://' + ticketlink;
                    }
                }
            });
            
            console.log(json);
			var uploadUrl = 'http://lkgtools.net/save/fansmvp/';

			$.ajax({			
			    type: 'POST',
			    url: uploadUrl,
			    dataType: 'json',			     
			    data: json,
			    success: function(data) {
                    $('#modal-message').html('New settings have been<br/>successfully submitted.');
                    $('#myModal').modal('show');
                    $('form')[0].reset();
                    $btn.button('reset');
			    },
			    error: function(jqXHR, textStatus, errorThrown) {
                    $('#modal-message').text(textStatus);
                    $('#myModal').modal('show');
                    $btn.button('reset');
				}
			});
			*/
        }
    });
});
